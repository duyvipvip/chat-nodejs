const path  = require('path');
var express = require('express');
var app     = express();
var serve   = require('http').createServer(app);

// ĐƯỜNG DẪN ĐẾN THƯ MỤC PUBLIC
const publicPath = path.join(__dirname, "../public");

// PORT ĐỂ TRUY CẬP APPLICATION
const port = process.env.port || 3000;

// SỬ DỤNG BUILT-IN MIDDLEWARE 
app.use(express.static(publicPath));

// KHỞI TẠO BIẾN SOCKET.IO
var io = require('socket.io')(serve);

// LẮNG NGHE KHI CLIENT KẾT NỐI
io.on('connection', (socket) => {
  console.log('Có một user kết nối');
  
  // LẮNG NGHE KHI CLINET Ở TRÊN NGẮT KẾT NỐI
  socket.on('disconnect', (socket) => {
    console.log('có một user ngắt kết nối');
  })

  // LẮNG NGHE KHI CLIENT GỬI TIN NHẮN
  socket.on('guitinnhan', (message) => {
    // GỦI TIN NHẮN CHO TẤT CẢ CÁC CLIENT
    socket.emit('guitinnhan', message.text);
  })
})

serve.listen(port, () =>{
  console.log(`serve hoạt động trên port ${port}`);
});


